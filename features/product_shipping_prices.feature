Feature: Show shipping prices on product page
  In order to increase conversion
  As a shop owner
  I show users minimal shipping price available for their address
  Or if address is not provided use default configured address for calculating shipping rates.



  Scenario: Showing minimal shipping rate from 2 shipping methods each has 2 rates available for customer primary address. Then showing all shipping rates from 2 shipping methods each has 2 rates available for customer primary address.
    Given user is logged in as "eric"
    Given user "eric" has shipping addresses:
      | id         | country | region      | postal_code | city   | street_address    |
      | address100 | PL      | Mazowieckie | 00-001      | Warsaw | Koszykowa 1       |
      | address105 | PL      | Mazowieckie | 00-002      | Warsaw | Nowogrodzka 22/15 |
    Given user "eric" has primary shipping address "address100"
    Given shipping methods are available for "product111" shipped to "PL", "Mazowieckie", "00-001", "Warsaw", "Koszykowa 1":
      | id     | title  |
      | dpd    | DPD    |
      | inpost | InPost |
    Given shipping rates for "product111" shipped to "PL", "Mazowieckie", "00-001", "Warsaw", "Koszykowa 1" with "dpd" are:
      | shipping_method_id | code        | price | title            | description       |
      | dpd                | standard    | 15.99 | Standard Courier | 3-5 business days |
      | dpd                | express     | 20.00 | Express Courier  | 2-3 business days |
    Given shipping rates for "product111" shipped to "PL", "Mazowieckie", "00-001", "Warsaw", "Koszykowa 1" with "inpost" are:
      | shipping_method_id | code        | price | title            | description       |
      | inpost             | packstation | 9.00  | Packstation      | 2-3 days          |
      | inpost             | courier     | 15.00 | Courier          | 2-3 days          |

    When user opens "product111" page
    Then shipping rate "inpost" "packstation" with title "InPost" "Packstation", price "9.00" and "2-3 days" is displayed



  Scenario: Showing all shipping rates from 2 shipping methods each has 2 rates available for customer primary address.
    Given user is logged in as "eric"
    Given user "eric" has shipping addresses:
      | id         | country | region      | postal_code | city   | street_address    |
      | address100 | PL      | Mazowieckie | 00-001      | Warsaw | Koszykowa 1       |
      | address105 | PL      | Mazowieckie | 00-002      | Warsaw | Nowogrodzka 22/15 |
    Given user "eric" has primary shipping address "address100"
    Given shipping methods are available for "product111" shipped to "PL", "Mazowieckie", "00-001", "Warsaw", "Koszykowa 1":
      | id     | title  |
      | dpd    | DPD    |
      | inpost | InPost |
    Given shipping rates for "product111" shipped to "PL", "Mazowieckie", "00-001", "Warsaw", "Koszykowa 1" with "dpd" are:
      | shipping_method_id | code        | price | title            | description       |
      | dpd                | standard    | 15.99 | Standard Courier | 3-5 business days |
      | dpd                | express     | 20.00 | Express Courier  | 2-3 business days |
    Given shipping rates for "product111" shipped to "PL", "Mazowieckie", "00-001", "Warsaw", "Koszykowa 1" with "inpost" are:
      | shipping_method_id | code        | price | title            | description       |
      | inpost             | packstation | 9.00  | Packstation      | 2-3 days          |
      | inpost             | courier     | 15.00 | Courier          | 2-3 days          |

    When user opens all shipping rates for "product111"
    Then shipping rates are displayed:
      | shipping_method_id| shipping_method_title | shipping_rate_code | shipping_rate_title | price | description       |
      | dpd               | DPD                   | standard           | Standard Courier    | 15.99 | 3-5 business days |
      | dpd               | DPD                   | express            | Express Courier     | 20.00 | 2-3 business days |
      | inpost            | InPost                | packstation        | Packstation         | 9.00  | 2-3 days          |
      | inpost            | InPost                | courier            | Courier             | 15.00 | 2-3 days          |



  Scenario: registered user can set session shipping address by providing postal code
    Given example shipping address is "PL", "Lubelskie", "01-222", "Lublin", "3 Maja 15"
    Given user is logged in as "eric"
    Given user "eric" has shipping addresses:
      | id         | country | region      | postal_code | city   | street_address    |
      | address100 | PL      | Mazowieckie | 00-001      | Warsaw | Koszykowa 1       |
      | address105 | PL      | Mazowieckie | 00-002      | Warsaw | Nowogrodzka 22/15 |
    Given user "eric" has primary shipping address "address100"

    When user sets session shipping address to "address105"
    Then user session address becomes "PL", "Mazowieckie", "00-002", "Warsaw", "Nowogrodzka 22/15"



  Scenario: user cannot set session shipping address to not known ID
    Given user "eric" has shipping addresses:
      | id         | country | region      | postal_code | city   | street_address    |
      | address100 | PL      | Mazowieckie | 00-001      | Warsaw | Koszykowa 1       |
      | address105 | PL      | Mazowieckie | 00-002      | Warsaw | Nowogrodzka 22/15 |
    Given user is logged in as "bob"

    When user sets session shipping address to "address100"
    Then such shipping address does not exist



  Scenario: user sets session shipping address by postal code
    Given example shipping address is "PL", "Lubelskie", "01-222", "Lublin", "3 Maja 15"
    Given postal code "00-001" is located in "Warsaw"
    Given user is not logged in

    When user sets session shipping postal code to "00-001"
    Then user session address becomes "PL", "00-001", "Warsaw"



  Scenario: user sets not existing postal code for session shipping address
    Given example shipping address is "PL", "Lubelskie", "01-222", "Lublin", "3 Maja 15"
    Given user is not logged in

    When user sets session shipping postal code to "00-001"
    Then such postal code does not exist

