<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\Mock\Service;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;
use Plugineria\ProductShippingPrice\Domain\Service\AvailableShippingMethodsResolver;
use Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository\InMemoryShippingMethodRepository;

class MockedAvailableShippingMethodsResolver implements AvailableShippingMethodsResolver
{
    /** @var array | ShippingMethod[] */
    private $shippingMethodIds = [];

    /** @var InMemoryShippingMethodRepository */
    private $shippingMethodRepository;

    public function __construct(InMemoryShippingMethodRepository $shippingMethodRepository)
    {
        $this->shippingMethodRepository = $shippingMethodRepository;
    }

    public function getAvailableShippingMethods(ProductId $productId, Address $shippingAddress): array
    {
        $shippingMethods = [];

        foreach ($this->shippingMethodIds as $shippingMethodId) {
            $shippingMethod = $this->shippingMethodRepository->findById($shippingMethodId);

            if (null !== $shippingMethod) {
                $shippingMethods[] = $shippingMethod;
            }
        }

        return $shippingMethods;
    }

    public function setShippingMethods(ShippingMethod ...$shippingMethods): void
    {
        $shippingMethodIds = [];
        foreach ($shippingMethods as $shippingMethod) {
            $this->shippingMethodRepository->save($shippingMethod);
            $shippingMethodIds[] = $shippingMethod->getId();
        }
        $this->shippingMethodIds = $shippingMethodIds;
    }
}
