<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\CustomerShippingAddressResolver;

class MockedCustomerShippingAddressResolver implements CustomerShippingAddressResolver
{
    /** @var CustomerShippingAddress | null */
    private $defaultCustomerShippingAddresses = [];

    public function getPrimaryCustomerShippingAddress(CustomerId $customerId): ?CustomerShippingAddress
    {
        return $this->defaultCustomerShippingAddresses[(string)$customerId] ?? null;
    }

    public function setDefaultCustomerShippingAddress(?CustomerShippingAddress $defaultCustomerShippingAddress): void
    {
        $customerId = (string)$defaultCustomerShippingAddress->getCustomerId();
        $this->defaultCustomerShippingAddresses[$customerId] = $defaultCustomerShippingAddress;
    }
}
