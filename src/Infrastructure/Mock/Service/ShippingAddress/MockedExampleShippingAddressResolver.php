<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\ExampleShippingAddressResolver;

class MockedExampleShippingAddressResolver implements ExampleShippingAddressResolver
{
    /** @var Address|null */
    private $address;

    public function getExampleShippingAddress(): Address
    {
        return $this->address;
    }

    public function setExampleShippingAddress(Address $address): void
    {
        $this->address = $address;
    }
}
