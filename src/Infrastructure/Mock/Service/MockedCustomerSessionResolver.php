<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\Mock\Service;

use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Service\CustomerSessionResolver;

class MockedCustomerSessionResolver implements CustomerSessionResolver
{
    /** @var CustomerId | null */
    private $customerId;

    public function getCustomerId(): ?CustomerId
    {
        return $this->customerId;
    }

    public function setCustomerId(?CustomerId $customerId): void
    {
        $this->customerId = $customerId;
    }
}
