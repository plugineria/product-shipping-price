<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\ShippingRate;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\PriceCalculator;

class MockedPriceCalculator implements PriceCalculator
{
    /** @var ShippingRate[][] */
    private $ratesPerShippingMethod = [];

    public function getRatesPerShippingMethod(
        ShippingMethodId $shippingMethodId,
        ProductId $productId,
        Address $shippingAddress
    ): array {
        return $this->ratesPerShippingMethod[(string)$shippingMethodId] ?? [];
    }

    public function setRatesPerShippingMethod(
        ShippingMethodId $shippingMethodId,
        ShippingRate ...$rates
    ): self {
        $this->ratesPerShippingMethod[(string)$shippingMethodId] = $rates;

        return $this;
    }
}
