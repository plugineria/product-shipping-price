<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\Mock\Factory;

use Plugineria\ProductShippingPrice\Domain\Exception\PostalCodeAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Factory\PostalCodeAddressFactory;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\Address\DefaultAddress;

class MockedPostalCodeAddressFactory implements PostalCodeAddressFactory
{
    /** @var string|null */
    private $city;

    public function create(string $postalCode, string $country): Address
    {
        if (null === $this->city) {
            throw new PostalCodeAddressNotFound($postalCode, $country);
        }

        return new DefaultAddress(
            $country,
            $this->city,
            $postalCode
        );
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }
}
