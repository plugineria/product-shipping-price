<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\InMemory\Service;

use Plugineria\ProductShippingPrice\App\Service\Cache;

use function time;

class InMemoryCache implements Cache
{
    private const VALUE = 'value';
    private const TTL = 'ttl';
    private const TIMESTAMP = 'timestamp';

    private $storage = [];

    public function get(string $key): ?string
    {
        if (!isset($this->storage[$key])) {
            return null;
        }
        if (
            null !== $this->storage[$key][self::TTL]
            && $this->storage[$key][self::TIMESTAMP] + $this->storage[$key][self::TTL] < time()
        ) {
            unset($this->storage[$key]);
            return null;
        }

        return $this->storage[$key][self::VALUE];
    }

    public function set(string $key, string $value, ?int $ttlSec = null): void
    {
        $this->storage[$key] = [
            self::VALUE => $value,
            self::TTL => $ttlSec,
            self::TIMESTAMP => time(),
        ];
    }

    public function clearAll(): void
    {
        $this->storage = [];
    }
}
