<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Repository\SessionShippingAddressRepository;

use function serialize;
use function unserialize;

class InMemorySessionShippingAddressRepository implements SessionShippingAddressRepository
{
    /** @var Address|null */
    private $address;

    public function get(): ?Address
    {
        return unserialize(serialize($this->address));
    }

    public function save(Address $address): void
    {
        $this->address = $address;
    }

    public function reset(): void
    {
        $this->address = null;
    }
}
