<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository;

use Plugineria\ProductShippingPrice\Domain\Exception\CustomerShippingAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddressId;
use Plugineria\ProductShippingPrice\Domain\Repository\CustomerShippingAddressRepository;

class InMemoryCustomerShippingAddressRepository implements CustomerShippingAddressRepository
{
    /** @var CustomerShippingAddress[] */
    private $addresses = [];

    public function getById(CustomerShippingAddressId $id): CustomerShippingAddress
    {
        $address = $this->findById($id);

        if (null === $address) {
            throw new CustomerShippingAddressNotFound($id);
        }

        return $address;
    }

    public function findById(CustomerShippingAddressId $id): ?CustomerShippingAddress
    {
        if (isset($this->addresses[(string)$id])) {
            return self::copy($this->addresses[(string)$id]);
        }

        return null;
    }

    public function save(CustomerShippingAddress $address): void
    {
        $this->addresses[(string)$address->getId()] = $address;
    }

    public function findAllByCustomerId(CustomerId $customerId): array
    {
        $addresses = [];
        foreach ($this->addresses as $address) {
            if ((string)$address->getCustomerId() === (string)$customerId) {
                $addresses[] = self::copy($address);
            }
        }

        return $addresses;
    }

    private static function copy(CustomerShippingAddress $address): CustomerShippingAddress
    {
        return unserialize(serialize($address));
    }
}
