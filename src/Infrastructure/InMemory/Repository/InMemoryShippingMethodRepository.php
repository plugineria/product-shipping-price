<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository;

use Plugineria\ProductShippingPrice\Domain\Exception\ShippingMethodNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Repository\ShippingMethodRepository;

class InMemoryShippingMethodRepository implements ShippingMethodRepository
{
    /** @var ShippingMethod[] */
    private $shippingMethods = [];

    public function findById(ShippingMethodId $shippingMethodId): ?ShippingMethod
    {
        if (isset($this->shippingMethods[(string)$shippingMethodId])) {
            return unserialize(serialize($this->shippingMethods[(string)$shippingMethodId]));
        }

        return null;
    }

    public function getById(ShippingMethodId $shippingMethodId): ShippingMethod
    {
        $shippingMethod = $this->findById($shippingMethodId);

        if (null === $shippingMethod) {
            throw new ShippingMethodNotFound($shippingMethodId);
        }

        return $shippingMethod;
    }

    public function save(ShippingMethod $shippingMethod): void
    {
        $this->shippingMethods[(string)$shippingMethod->getId()] = $shippingMethod;
    }
}
