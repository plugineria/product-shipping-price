<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\View;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddressId;

class SessionShippingAddressView
{
    /** @var Address */
    private $address;

    /** @var CustomerShippingAddressId|null */
    private $customerShippingAddressId;

    /** @var CustomerShippingAddress[] */
    private $customerShippingAddresses = [];

    /** @var bool */
    private $isProvidedByCustomer;

    public function __construct(
        Address $address,
        ?CustomerShippingAddressId $customerShippingAddressId,
        array $customerShippingAddresses,
        bool $isProvidedByCustomer
    ) {
        $this->address = $address;
        $this->customerShippingAddressId = $customerShippingAddressId;
        $this->customerShippingAddresses = $customerShippingAddresses;
        $this->isProvidedByCustomer = $isProvidedByCustomer;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function getCustomerShippingAddressId(): ?CustomerShippingAddressId
    {
        return $this->customerShippingAddressId;
    }

    public function isProvidedByCustomer(): bool
    {
        return $this->isProvidedByCustomer;
    }

    public function getCustomerShippingAddresses(): array
    {
        return $this->customerShippingAddresses;
    }
}
