<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\View;

use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;

class ShippingRateView
{
    /** @var ShippingMethod  */
    private $shippingMethod;

    /** @var float */
    private $price;

    /** @var string */
    private $code;

    /** @var string|null */
    private $title;

    /** @var string|null */
    private $description;

    public function __construct(
        ShippingMethod $shippingMethod,
        float $price,
        string $code,
        ?string $title = null,
        ?string $description = null
    ) {
        $this->shippingMethod = $shippingMethod;
        $this->price = $price;
        $this->code = $code;
        $this->title = $title;
        $this->description = $description;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getShippingMethod(): ShippingMethod
    {
        return $this->shippingMethod;
    }
}
