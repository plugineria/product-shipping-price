<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\View;

use Plugineria\ProductShippingPrice\Domain\Exception\ShippingMethodNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;
use Plugineria\ProductShippingPrice\Domain\Repository\ShippingMethodRepository;

class ShippingRateViewFactory
{
    /** @var ShippingMethodRepository */
    private $shippingMethodRepository;

    public function __construct(ShippingMethodRepository $shippingMethodRepository)
    {
        $this->shippingMethodRepository = $shippingMethodRepository;
    }

    /**
     * @throws ShippingMethodNotFound
     */
    public function create(ShippingRate $shippingRate): ShippingRateView
    {
        return new ShippingRateView(
            $this->shippingMethodRepository->getById($shippingRate->getShippingMethodId()),
            $shippingRate->getPrice(),
            $shippingRate->getCode(),
            $shippingRate->getTitle(),
            $shippingRate->getDescription(),
        );
    }
}
