<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Service;

interface Cache
{
    public function get(string $key): ?string;

    public function set(string $key, string $value, ?int $ttlSec = null): void;
}
