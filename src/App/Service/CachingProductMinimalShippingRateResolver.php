<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Service;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\ProductMinimalShippingRateResolver;

use function md5;
use function serialize;
use function sprintf;
use function unserialize;

class CachingProductMinimalShippingRateResolver implements ProductMinimalShippingRateResolver
{
    private const CACHE_KEY_PATTERN = 'widgento.product_shipping_price.min_price.%s';

    /** @var ProductMinimalShippingRateResolver */
    private $realResolver;

    /** @var Cache */
    private $cache;

    /** @var int */
    private $ttl;

    public function __construct(ProductMinimalShippingRateResolver $realResolver, Cache $cache, int $ttl)
    {
        $this->realResolver = $realResolver;
        $this->cache = $cache;
        $this->ttl = $ttl;
    }

    public function getMinimalShippingRate(ProductId $productId, ?Address $shippingAddress = null): ?ShippingRate
    {
        $canCache = $this->canCache($shippingAddress);
        $cacheKey = $this->getCacheKey($productId, $shippingAddress);
        if ($canCache) {
            /** @var ShippingRate $minimalShippingRate */
            $cachedShippingRate = $this->cache->get($cacheKey);
            $minimalShippingRate = $cachedShippingRate ? unserialize($cachedShippingRate) : null;

            if (null !== $minimalShippingRate) {
                return $minimalShippingRate;
            }
        }

        $minimalShippingRate = $this->realResolver->getMinimalShippingRate($productId, $shippingAddress);

        if ($canCache && $minimalShippingRate) {
            $this->cache->set($cacheKey, serialize($minimalShippingRate), $this->ttl);
        }

        return $minimalShippingRate;
    }

    private function getCacheKey(ProductId $productId, ?Address $shippingAddress): string
    {
        $identity = (string)$productId . '.' . md5(serialize($shippingAddress));

        return sprintf(self::CACHE_KEY_PATTERN, $identity);
    }

    private function canCache(?Address $customerAddress): bool
    {
        return null !== $customerAddress;
    }
}
