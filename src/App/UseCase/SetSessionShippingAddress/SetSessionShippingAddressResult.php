<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress;

class SetSessionShippingAddressResult
{
    public const ADDRESS_NOT_SPECIFIED = 'address_not_specified';
    public const CUSTOMER_ADDRESS_ID_NOT_FOUND = 'customer_address_id_not_found';
    public const POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY = 'postal_code_does_not_exist_in_country';

    /** @var bool */
    private $isValid;

    /** @var string|null */
    private $error;

    private function __construct(bool $isValid = true, ?string $error = null)
    {
        $this->isValid = $isValid;
        $this->error = $error;
    }

    public function isValid(): bool
    {
        return $this->isValid;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public static function createValid(): self
    {
        return new self();
    }

    public static function createInvalidWithAddressNotSpecified(): self
    {
        return new self(false, self::ADDRESS_NOT_SPECIFIED);
    }

    public static function createInvalidWithCustomerAddressIdNotFound(): self
    {
        return new self(false, self::CUSTOMER_ADDRESS_ID_NOT_FOUND);
    }

    public static function createInvalidWithPostalCodeDoesNotExistInCountry(): self
    {
        return new self(false, self::POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY);
    }
}
