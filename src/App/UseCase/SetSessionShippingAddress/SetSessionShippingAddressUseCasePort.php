<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress;

interface SetSessionShippingAddressUseCasePort
{
    public function execute(SetSessionShippingAddressCommand $command): SetSessionShippingAddressResult;
}
