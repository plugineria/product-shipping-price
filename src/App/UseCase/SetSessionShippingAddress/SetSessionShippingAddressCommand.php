<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddressId;

class SetSessionShippingAddressCommand
{
    /** @var string|null */
    private $country;

    /** @var string|null */
    private $postalCode;

    /** @var CustomerShippingAddressId|null */
    private $customerShippingAddressId;

    public function __construct(
        ?string $country = null,
        ?string $postalCode = null,
        ?CustomerShippingAddressId $customerShippingAddressId = null
    ) {
        $this->country = $country;
        $this->postalCode = $postalCode;
        $this->customerShippingAddressId = $customerShippingAddressId;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function getCustomerShippingAddressId(): ?CustomerShippingAddressId
    {
        return $this->customerShippingAddressId;
    }
}
