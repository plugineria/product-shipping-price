<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Exception\CustomerShippingAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Exception\PostalCodeAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\SessionCustomerShippingAddressUpdater;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\SessionPostalCodeAddressUpdater;

class SetSessionShippingAddressUseCase implements SetSessionShippingAddressUseCasePort
{
    /** @var SessionCustomerShippingAddressUpdater */
    private $customerShippingAddressUpdater;

    /** @var SessionPostalCodeAddressUpdater */
    private $postalCodeUpdater;

    public function __construct(
        SessionCustomerShippingAddressUpdater $sessionCustomerShippingAddressUpdater,
        SessionPostalCodeAddressUpdater $sessionPostalCodeUpdater
    ) {
        $this->customerShippingAddressUpdater = $sessionCustomerShippingAddressUpdater;
        $this->postalCodeUpdater = $sessionPostalCodeUpdater;
    }

    public function execute(SetSessionShippingAddressCommand $command): SetSessionShippingAddressResult
    {
        if (null !== $command->getCustomerShippingAddressId()) {
            try {
                $this->customerShippingAddressUpdater->update($command->getCustomerShippingAddressId());
            } catch (CustomerShippingAddressNotFound $e) {
                return SetSessionShippingAddressResult::createInvalidWithCustomerAddressIdNotFound();
            }
        } elseif (null !== $command->getPostalCode()) {
            try {
                $this->postalCodeUpdater->update($command->getPostalCode(), $command->getCountry());
            } catch (PostalCodeAddressNotFound $e) {
                return SetSessionShippingAddressResult::createInvalidWithPostalCodeDoesNotExistInCountry();
            }
        } else {
            return SetSessionShippingAddressResult::createInvalidWithAddressNotSpecified();
        }

        return SetSessionShippingAddressResult::createValid();
    }
}
