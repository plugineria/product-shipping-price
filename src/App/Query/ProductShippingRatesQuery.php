<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Query;

use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\App\View\ShippingRateViewFactory;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\ShippingAddressResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\ProductShippingRatesResolver;

class ProductShippingRatesQuery implements ProductShippingRatesQueryPort
{
    /** @var ProductShippingRatesResolver */
    private $productShippingRatesResolver;

    /** @var ShippingAddressResolver */
    private $shippingAddressResolver;

    /** @var ShippingRateViewFactory */
    private $shippingRateViewFactory;

    public function __construct(
        ProductShippingRatesResolver $productShippingRatesResolver,
        ShippingAddressResolver $shippingAddressResolver,
        ShippingRateViewFactory $shippingRateViewFactory
    ) {
        $this->productShippingRatesResolver = $productShippingRatesResolver;
        $this->shippingAddressResolver = $shippingAddressResolver;
        $this->shippingRateViewFactory = $shippingRateViewFactory;
    }

    /**
     * @param ProductId $productId
     *
     * @return ShippingRateView[]
     */
    public function execute(ProductId $productId): array
    {
        $shippingRates = $this->productShippingRatesResolver->getProductShippingRates(
            $productId,
            $this->shippingAddressResolver->getDefaultShippingAddress()
        );

        return array_map(
            function (ShippingRate $shippingRate): ShippingRateView {
                return $this->shippingRateViewFactory->create($shippingRate);
            },
            $shippingRates
        );
    }
}
