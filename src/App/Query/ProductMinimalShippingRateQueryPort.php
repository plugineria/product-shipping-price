<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Query;

use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;

interface ProductMinimalShippingRateQueryPort
{
    public function execute(ProductId $productId): ?ShippingRateView;
}
