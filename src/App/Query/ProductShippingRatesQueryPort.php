<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Query;

use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;

interface ProductShippingRatesQueryPort
{
    /**
     * @param ProductId $productId
     *
     * @return ShippingRateView[]
     */
    public function execute(ProductId $productId): array;
}
