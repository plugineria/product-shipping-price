<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Query;

use Plugineria\ProductShippingPrice\App\View\SessionShippingAddressView;
use Plugineria\ProductShippingPrice\Domain\Repository\CustomerShippingAddressRepository;
use Plugineria\ProductShippingPrice\Domain\Service\CustomerSessionResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\ExampleShippingAddressResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\ShippingAddressResolver;

class SessionShippingAddressQuery implements SessionShippingAddressQueryPort
{
    /** @var ShippingAddressResolver */
    private $shippingAddressResolver;

    /** @var CustomerShippingAddressRepository */
    private $customerShippingAddressRepository;

    /** @var CustomerSessionResolver */
    private $customerSessionResolver;

    /** @var ExampleShippingAddressResolver */
    private $exampleShippingAddressResolver;

    public function __construct(
        ShippingAddressResolver $shippingAddressResolver,
        CustomerShippingAddressRepository $customerShippingAddressRepository,
        CustomerSessionResolver $customerSessionResolver,
        ExampleShippingAddressResolver $exampleShippingAddressResolver
    ) {
        $this->shippingAddressResolver = $shippingAddressResolver;
        $this->customerShippingAddressRepository = $customerShippingAddressRepository;
        $this->customerSessionResolver = $customerSessionResolver;
        $this->exampleShippingAddressResolver = $exampleShippingAddressResolver;
    }

    public function execute(): SessionShippingAddressView
    {
        $address = $this->shippingAddressResolver->getDefaultShippingAddress();
        $customerId = $this->customerSessionResolver->getCustomerId();
        $customerShippingAddresses = $customerId
            ? $this->customerShippingAddressRepository->findAllByCustomerId($customerId)
            : [];

        $customerShippingAddressId = null;
        foreach ($customerShippingAddresses as $customerShippingAddress) {
            if ($customerShippingAddress->getAddress()->equals($address)) {
                $customerShippingAddressId = $customerShippingAddress->getId();
                break;
            }
        }

        return new SessionShippingAddressView(
            $address,
            $customerShippingAddressId,
            $customerShippingAddresses,
            !$this->exampleShippingAddressResolver->getExampleShippingAddress()->equals($address)
        );
    }
}
