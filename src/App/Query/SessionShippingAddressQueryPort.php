<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Query;

use Plugineria\ProductShippingPrice\App\View\SessionShippingAddressView;

interface SessionShippingAddressQueryPort
{
    public function execute(): SessionShippingAddressView;
}
