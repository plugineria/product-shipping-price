<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Query;

use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\App\View\ShippingRateViewFactory;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\ShippingAddressResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\ProductMinimalShippingRateResolver;

class ProductMinimalShippingRateQuery implements ProductMinimalShippingRateQueryPort
{
    /** @var ProductMinimalShippingRateResolver */
    private $productMinimalShippingRateResolver;

    /** @var ShippingAddressResolver */
    private $shippingAddressResolver;

    /** @var ShippingRateViewFactory */
    private $shippingRateViewFactory;

    public function __construct(
        ProductMinimalShippingRateResolver $productMinimalShippingRateResolver,
        ShippingAddressResolver $shippingAddressResolver,
        ShippingRateViewFactory $shippingRateViewFactory
    ) {
        $this->productMinimalShippingRateResolver = $productMinimalShippingRateResolver;
        $this->shippingAddressResolver = $shippingAddressResolver;
        $this->shippingRateViewFactory = $shippingRateViewFactory;
    }

    public function execute(ProductId $productId): ?ShippingRateView
    {
        $shippingRate = $this->productMinimalShippingRateResolver->getMinimalShippingRate(
            $productId,
            $this->shippingAddressResolver->getDefaultShippingAddress()
        );

        return $shippingRate ? $this->shippingRateViewFactory->create($shippingRate) : null;
    }
}
