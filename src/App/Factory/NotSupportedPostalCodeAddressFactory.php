<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\App\Factory;

use Plugineria\ProductShippingPrice\Domain\Exception\PostalCodeAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Factory\PostalCodeAddressFactory;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;

class NotSupportedPostalCodeAddressFactory implements PostalCodeAddressFactory
{
    public function create(string $postalCode, string $country): Address
    {
        throw new PostalCodeAddressNotFound($postalCode, $country);
    }
}
