<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Exception;

use LogicException;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddressId;

class CustomerShippingAddressNotFound extends LogicException
{
    /** @var CustomerShippingAddressId */
    private $customerShippingAddressId;

    public function __construct(CustomerShippingAddressId $customerShippingAddressId)
    {
        parent::__construct("Customer shipping address {$this->customerShippingAddressId} not found");
        $this->customerShippingAddressId = $customerShippingAddressId;
    }

    public function getCustomerShippingAddressId(): CustomerShippingAddressId
    {
        return $this->customerShippingAddressId;
    }
}
