<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Exception;

use LogicException;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;

class ShippingMethodNotFound extends LogicException
{
    /** @var $shippingMethodId */
    private $shippingMethodId;

    public function __construct(ShippingMethodId $shippingMethodId)
    {
        parent::__construct("Shipping method $shippingMethodId not found");

        $this->shippingMethodId = $shippingMethodId;
    }

    public function getShippingMethodId(): ShippingMethodId
    {
        return $this->shippingMethodId;
    }
}
