<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Exception;

use Throwable;
use UnexpectedValueException;

class AddressAttributeNotFound extends UnexpectedValueException
{
    /** @var string */
    private $attributeName;

    public function __construct(string $attributeName, Throwable $previous = null)
    {
        parent::__construct("Address attribute $attributeName not found", 0, $previous);
    }

    public function getAttributeName(): string
    {
        return $this->attributeName;
    }
}
