<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Exception;

use LogicException;

class PostalCodeAddressNotFound extends LogicException
{
    /** @var string */
    private $postalCode;

    /** @var string */
    private $country;

    public function __construct(string $postalCode, string $country)
    {
        parent::__construct("Address for postal code: {$this->postalCode} in country {$this->country} not found");
        $this->postalCode = $postalCode;
        $this->country = $country;
    }
}
