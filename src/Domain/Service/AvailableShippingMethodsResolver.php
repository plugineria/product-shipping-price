<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;

interface AvailableShippingMethodsResolver
{
    /**
     * @return ShippingMethod[]|array
     */
    public function getAvailableShippingMethods(ProductId $productId, Address $shippingAddress): array;
}
