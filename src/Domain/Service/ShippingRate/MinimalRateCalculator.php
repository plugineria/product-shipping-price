<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingRate;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;

class MinimalRateCalculator
{
    /** @var PriceCalculator */
    private $priceCalculator;

    public function __construct(PriceCalculator $priceCalculator)
    {
        $this->priceCalculator = $priceCalculator;
    }

    public function calculate(
        ProductId $productId,
        ShippingMethodId $shippingMethodId,
        ?Address $shippingAddress
    ): ?ShippingRate {
        $shippingRates = $this->priceCalculator->getRatesPerShippingMethod(
            $shippingMethodId,
            $productId,
            $shippingAddress
        );

        if (empty($shippingRates)) {
            return null;
        }

        /** @var ShippingRate|null $minimalRate */
        $minimalRate = null;

        foreach ($shippingRates as $shippingRate) {
            if (null === $minimalRate) {
                $minimalRate = $shippingRate;
            } elseif ($shippingRate->getPrice() < $minimalRate->getPrice()) {
                $minimalRate = $shippingRate;
            }
        }

        return $minimalRate;
    }
}
