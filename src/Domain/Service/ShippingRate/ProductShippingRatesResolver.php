<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingRate;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;
use Plugineria\ProductShippingPrice\Domain\Service\AvailableShippingMethodsResolver;

class ProductShippingRatesResolver
{
    /** @var AvailableShippingMethodsResolver */
    private $availableShippingMethodsResolver;

    /** @var PriceCalculator */
    private $priceCalculator;

    public function __construct(
        AvailableShippingMethodsResolver $availableShippingMethodsResolver,
        PriceCalculator $priceCalculator
    ) {
        $this->availableShippingMethodsResolver = $availableShippingMethodsResolver;
        $this->priceCalculator = $priceCalculator;
    }

    /**
     * @return ShippingRate[]
     */
    public function getProductShippingRates(ProductId $productId, ?Address $shippingAddress = null): array
    {
        $shippingRates = [[]];

        $shippingMethods = $this->availableShippingMethodsResolver->getAvailableShippingMethods(
            $productId,
            $shippingAddress
        );

        foreach ($shippingMethods as $shippingMethod) {
            $shippingRates[] = $this->priceCalculator->getRatesPerShippingMethod(
                $shippingMethod->getId(),
                $productId,
                $shippingAddress
            );
        }

        return array_merge(...$shippingRates);
    }
}
