<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingRate;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;
use Plugineria\ProductShippingPrice\Domain\Service\AvailableShippingMethodsResolver;

class DefaultProductMinimalShippingRateResolver implements ProductMinimalShippingRateResolver
{
    /** @var AvailableShippingMethodsResolver */
    private $availableShippingMethodsResolver;

    /** @var MinimalRateCalculator */
    private $minimalRateCalculator;

    public function __construct(
        AvailableShippingMethodsResolver $availableShippingMethodsResolver,
        MinimalRateCalculator $minimalRateCalculator
    ) {
        $this->availableShippingMethodsResolver = $availableShippingMethodsResolver;
        $this->minimalRateCalculator = $minimalRateCalculator;
    }

    public function getMinimalShippingRate(ProductId $productId, ?Address $shippingAddress = null): ?ShippingRate
    {
        $minimalShippingRate = null;
        $availableShippingMethods = $this->availableShippingMethodsResolver->getAvailableShippingMethods(
            $productId,
            $shippingAddress
        );
        foreach ($availableShippingMethods as $shippingMethod) {
            $shippingRate = $this->minimalRateCalculator->calculate(
                $productId,
                $shippingMethod->getId(),
                $shippingAddress
            );

            if (null === $minimalShippingRate) {
                $minimalShippingRate = $shippingRate;
            } elseif (null !== $shippingRate && $shippingRate->getPrice() < $minimalShippingRate->getPrice()) {
                $minimalShippingRate = $shippingRate;
            }
        }

        return $minimalShippingRate;
    }
}
