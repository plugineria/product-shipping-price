<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingRate;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;

interface ProductMinimalShippingRateResolver
{
    public function getMinimalShippingRate(ProductId $productId, ?Address $shippingAddress = null): ?ShippingRate;
}
