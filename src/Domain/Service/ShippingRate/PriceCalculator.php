<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingRate;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;

interface PriceCalculator
{
    /**
     * @return ShippingRate[]|array
     */
    public function getRatesPerShippingMethod(
        ShippingMethodId $shippingMethodId,
        ProductId $productId,
        Address $shippingAddress
    ): array;
}
