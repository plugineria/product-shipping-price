<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;

interface ExampleShippingAddressResolver
{
    public function getExampleShippingAddress(): Address;
}
