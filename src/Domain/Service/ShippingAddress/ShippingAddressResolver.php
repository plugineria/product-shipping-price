<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;

interface ShippingAddressResolver
{
    public function getDefaultShippingAddress(): ?Address;
}
