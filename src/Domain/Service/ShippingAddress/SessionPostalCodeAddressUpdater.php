<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Exception\PostalCodeAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Factory\PostalCodeAddressFactory;
use Plugineria\ProductShippingPrice\Domain\Repository\SessionShippingAddressRepository;

class SessionPostalCodeAddressUpdater
{
    /** @var SessionShippingAddressRepository */
    private $sessionShippingAddressRepository;

    /** @var PostalCodeAddressFactory */
    private $postalCodeAddressFactory;

    /** @var ExampleShippingAddressResolver */
    private $exampleShippingAddressResolver;

    public function __construct(
        SessionShippingAddressRepository $sessionShippingAddressRepository,
        PostalCodeAddressFactory $postalCodeAddressFactory,
        ExampleShippingAddressResolver $exampleShippingAddressResolver
    ) {
        $this->sessionShippingAddressRepository = $sessionShippingAddressRepository;
        $this->postalCodeAddressFactory = $postalCodeAddressFactory;
        $this->exampleShippingAddressResolver = $exampleShippingAddressResolver;
    }

    /**
     * @throws PostalCodeAddressNotFound
     */
    public function update(string $postalCode, ?string $country): void
    {
        $shippingAddress = $this->postalCodeAddressFactory->create(
            $postalCode,
            $country ?: $this->exampleShippingAddressResolver->getExampleShippingAddress()->getCountry()
        );

        $this->sessionShippingAddressRepository->save($shippingAddress);
    }
}
