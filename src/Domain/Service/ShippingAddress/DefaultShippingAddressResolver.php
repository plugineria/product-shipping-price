<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Repository\SessionShippingAddressRepository;
use Plugineria\ProductShippingPrice\Domain\Service\CustomerSessionResolver;

class DefaultShippingAddressResolver implements ShippingAddressResolver
{
    /** @var SessionShippingAddressRepository */
    private $sessionShippingAddressRepository;

    /** @var CustomerShippingAddressResolver */
    private $customerShippingAddressResolver;

    /** @var CustomerSessionResolver */
    private $customerSessionResolver;

    /** @var ExampleShippingAddressResolver */
    private $exampleShippingAddressResolver;

    public function __construct(
        SessionShippingAddressRepository $sessionShippingAddressRepository,
        CustomerShippingAddressResolver $customerShippingAddressResolver,
        CustomerSessionResolver $customerSessionResolver,
        ExampleShippingAddressResolver $exampleShippingAddressResolver
    ) {
        $this->sessionShippingAddressRepository = $sessionShippingAddressRepository;
        $this->customerShippingAddressResolver = $customerShippingAddressResolver;
        $this->customerSessionResolver = $customerSessionResolver;
        $this->exampleShippingAddressResolver = $exampleShippingAddressResolver;
    }

    public function getDefaultShippingAddress(): ?Address
    {
        $sessionAddress = $this->sessionShippingAddressRepository->get();

        if (null !== $sessionAddress) {
            return $sessionAddress;
        }

        $customerId = $this->customerSessionResolver->getCustomerId();
        $customerShippingAddress = $customerId
            ? $this->customerShippingAddressResolver->getPrimaryCustomerShippingAddress($customerId)
            : null;

        if (null === $customerShippingAddress) {
            $shippingAddress = $this->exampleShippingAddressResolver->getExampleShippingAddress();
        } else {
            $shippingAddress = $customerShippingAddress->getAddress();
            $this->sessionShippingAddressRepository->save($shippingAddress);
        }

        return $shippingAddress;
    }
}
