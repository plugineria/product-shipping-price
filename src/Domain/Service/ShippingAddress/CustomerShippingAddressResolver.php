<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddress;

interface CustomerShippingAddressResolver
{
    public function getPrimaryCustomerShippingAddress(CustomerId $customerId): ?CustomerShippingAddress;
}
