<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Exception\CustomerShippingAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddressId;
use Plugineria\ProductShippingPrice\Domain\Repository\CustomerShippingAddressRepository;
use Plugineria\ProductShippingPrice\Domain\Repository\SessionShippingAddressRepository;
use Plugineria\ProductShippingPrice\Domain\Service\CustomerSessionResolver;

class SessionCustomerShippingAddressUpdater
{
    /** @var SessionShippingAddressRepository */
    private $sessionShippingAddressRepository;

    /** @var CustomerShippingAddressRepository */
    private $customerShippingAddressRepository;

    /** @var CustomerSessionResolver */
    private $customerSessionResolver;

    public function __construct(
        SessionShippingAddressRepository $sessionShippingAddressRepository,
        CustomerShippingAddressRepository $customerShippingAddressRepository,
        CustomerSessionResolver $customerSessionResolver
    ) {
        $this->sessionShippingAddressRepository = $sessionShippingAddressRepository;
        $this->customerShippingAddressRepository = $customerShippingAddressRepository;
        $this->customerSessionResolver = $customerSessionResolver;
    }

    /**
     * @throws CustomerShippingAddressNotFound
     */
    public function update(CustomerShippingAddressId $addressId): void
    {
        $customerAddress = $this->customerShippingAddressRepository->getById($addressId);

        if ((string)$customerAddress->getCustomerId() !== (string)$this->customerSessionResolver->getCustomerId()) {
            throw new CustomerShippingAddressNotFound($addressId);
        }

        $this->sessionShippingAddressRepository->save($customerAddress->getAddress());
    }
}
