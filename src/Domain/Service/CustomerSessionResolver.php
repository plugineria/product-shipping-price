<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Service;

use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;

interface CustomerSessionResolver
{
    public function getCustomerId(): ?CustomerId;
}
