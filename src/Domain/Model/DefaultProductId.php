<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model;

class DefaultProductId implements ProductId
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
