<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\Address;

interface Address
{
    public function getCountry(): string;

    public function getPostalCode(): ?string;

    public function getCity(): string;

    public function getStreetAddress(): ?string;

    public function getRegion(): ?string;

    public function equals(self $address): bool;

    public function __toString(): string;

    public static function createFromJson(string $json): self;
}
