<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\Address;

use JsonException;
use Plugineria\ProductShippingPrice\Domain\Exception\AddressAttributeNotFound;

use function json_decode;
use function json_encode;

class DefaultAddress implements Address
{
    public const COUNTRY = 'country';
    public const CITY = 'city';
    public const POSTAL_CODE = 'postal_code';
    public const STREET_ADDRESS = 'street_address';
    public const REGION = 'region';

    /** @var string */
    private $country;

    /** @var string */
    private $city;

    /** @var string | null */
    private $postalCode;

    /** @var string | null */
    private $streetAddress;

    /** @var string | null */
    private $region;

    public function __construct(
        string $country,
        string $city,
        ?string $postalCode = null,
        ?string $streetAddress = null,
        ?string $region = null
    ) {
        $this->country = $country;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->streetAddress = $streetAddress;
        $this->region = $region;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function getStreetAddress(): ?string
    {
        return $this->streetAddress;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function equals(Address $address): bool
    {
        return $this->getCountry() === $address->getCountry()
            && $this->getPostalCode() === $address->getPostalCode()
            && $this->getCity() === $address->getCity()
            && $this->getStreetAddress() === $address->getStreetAddress();
    }

    public function toArray(): array
    {
        return [
            self::COUNTRY => $this->getCountry(),
            self::CITY => $this->getCity(),
            self::POSTAL_CODE => $this->getPostalCode(),
            self::STREET_ADDRESS => $this->getStreetAddress(),
            self::REGION => $this->getRegion(),
        ];
    }

    public function __toString(): string
    {
        return json_encode($this->toArray());
    }

     /**
     * @param string $json
      *
      * @return DefaultAddress
      *
      * @throws JsonException
     * @throws AddressAttributeNotFound
     */
    public static function createFromJson(string $json): Address
    {
        $array = json_decode($json, true, 2, JSON_THROW_ON_ERROR);

        if (!isset($array[self::COUNTRY])) {
            throw new AddressAttributeNotFound(self::COUNTRY);
        }

        if (!isset($array[self::CITY])) {
            throw new AddressAttributeNotFound(self::CITY);
        }

        return new self(
            $array[self::COUNTRY],
            $array[self::CITY],
            $array[self::POSTAL_CODE] ?? null,
            $array[self::STREET_ADDRESS] ?? null,
            $array[self::REGION] ?? null,
        );
    }
}
