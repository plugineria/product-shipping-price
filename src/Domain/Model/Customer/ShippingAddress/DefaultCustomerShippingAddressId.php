<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress;

class DefaultCustomerShippingAddressId implements CustomerShippingAddressId
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
