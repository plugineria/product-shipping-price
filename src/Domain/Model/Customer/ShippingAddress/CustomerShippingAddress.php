<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;

interface CustomerShippingAddress
{
    public function getId(): CustomerShippingAddressId;
    public function getCustomerId(): CustomerId;
    public function getAddress(): Address;
}
