<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;

class DefaultCustomerShippingAddress implements CustomerShippingAddress
{
    /** @var CustomerShippingAddressId */
    private $id;

    /** @var Address */
    private $address;

    /** @var CustomerId */
    private $customerId;

    public function __construct(CustomerShippingAddressId $id, Address $address, CustomerId $customerId)
    {
        $this->id = $id;
        $this->address = $address;
        $this->customerId = $customerId;
    }

    public function getId(): CustomerShippingAddressId
    {
        return $this->id;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function getCustomerId(): CustomerId
    {
        return $this->customerId;
    }
}
