<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress;

interface CustomerShippingAddressId
{
    public function __toString(): string;
}
