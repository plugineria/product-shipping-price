<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\Customer;

interface CustomerId
{
    public function __toString(): string;
}
