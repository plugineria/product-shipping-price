<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod;

class DefaultShippingMethod implements ShippingMethod
{
    /** @var ShippingMethodId */
    private $id;

    /** @var string */
    private $title;

    public function __construct(ShippingMethodId $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    public function getId(): ShippingMethodId
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
