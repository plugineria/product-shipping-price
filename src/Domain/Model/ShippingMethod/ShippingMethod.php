<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod;

interface ShippingMethod
{
    public function getId(): ShippingMethodId;

    public function getTitle(): string;
}
