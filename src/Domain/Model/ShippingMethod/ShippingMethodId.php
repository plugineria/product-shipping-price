<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod;

interface ShippingMethodId
{
    public function __toString(): string;
}
