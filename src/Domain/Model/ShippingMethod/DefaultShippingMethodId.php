<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod;

class DefaultShippingMethodId implements ShippingMethodId
{
    /** @var string */
    private $code;

    public function __construct(string $code)
    {
        $this->code = $code;
    }

    public function __toString(): string
    {
        return $this->code;
    }
}
