<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model;

interface ProductId
{
    public function __toString(): string;
}
