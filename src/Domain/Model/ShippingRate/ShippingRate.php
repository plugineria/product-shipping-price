<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\ShippingRate;

use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;

interface ShippingRate
{
    public function getPrice(): float;

    public function getTitle(): ?string;

    public function getDescription(): ?string;

    public function getShippingMethodId(): ShippingMethodId;

    public function getCode(): string;
}
