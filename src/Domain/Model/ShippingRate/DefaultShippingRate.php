<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Model\ShippingRate;

use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;

class DefaultShippingRate implements ShippingRate
{
    /** @var ShippingMethodId  */
    private $shippingMethodId;

    /** @var string */
    private $code;

    /** @var float */
    private $price;

    /** @var string|null */
    private $title;

    /** @var string|null */
    private $description;

    public function __construct(
        ShippingMethodId $shippingMethodId,
        string $code,
        float $price,
        ?string $title = null,
        ?string $description = null
    ) {
        $this->shippingMethodId = $shippingMethodId;
        $this->code = $code;
        $this->price = $price;
        $this->title = $title;
        $this->description = $description;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getShippingMethodId(): ShippingMethodId
    {
        return $this->shippingMethodId;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
