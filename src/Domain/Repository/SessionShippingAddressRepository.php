<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Repository;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;

interface SessionShippingAddressRepository
{
    public function get(): ?Address;
    public function save(Address $address): void;
}
