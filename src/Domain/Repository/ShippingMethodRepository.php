<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Repository;

use Plugineria\ProductShippingPrice\Domain\Exception\ShippingMethodNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;

interface ShippingMethodRepository
{
    /**
     * @throws ShippingMethodNotFound
     */
    public function getById(ShippingMethodId $shippingMethodId): ShippingMethod;

    public function findById(ShippingMethodId $shippingMethodId): ?ShippingMethod;
}
