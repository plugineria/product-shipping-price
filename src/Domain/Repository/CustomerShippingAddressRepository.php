<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Repository;

use Plugineria\ProductShippingPrice\Domain\Exception\CustomerShippingAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddressId;

interface CustomerShippingAddressRepository
{
    public function findById(CustomerShippingAddressId $id): ?CustomerShippingAddress;

    /**
     * @throws CustomerShippingAddressNotFound
     */
    public function getById(CustomerShippingAddressId $id): CustomerShippingAddress;

    /**
     * @param CustomerId $customerId
     *
     * @return CustomerShippingAddress[]
     */
    public function findAllByCustomerId(CustomerId $customerId): array;
}
