<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Domain\Factory;

use Plugineria\ProductShippingPrice\Domain\Exception\PostalCodeAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;

interface PostalCodeAddressFactory
{
    /**
     * @throws PostalCodeAddressNotFound
     */
    public function create(string $postalCode, string $country): Address;
}
