# Product Shipping Price

In order to increase conversion  
As a shop owner  
I show users minimal shipping price available for their address  
Or if address is not provided use default configured address for calculating shipping rates.

Detailed scenarios in [feature specification](https://gitlab.com/plugineria/product-shipping-price/-/blob/master/features/product_shipping_prices.feature)

## Glossary

* **Customer Shipping Address** - address of a registered customer that is stored in a database and optionally marked as shipping address.
* **Primary Customer Shipping Address** - registered customer's main address for shipping stored in a database.
* **Session Shipping Address** - Address or shipping area (by postal code) provided by a registered customer or a guest user and user for shipping rates calculation. Guest user can only specify shipping area by providing postal code. Registered customer can choose one of saved customer shipping addresses or specify postal code. Session address stored within session only
* **Example Shipping Address** - Shipping address provided by a store owner in order to show default shipping rates without input information from user. Country from example shipping address is used for finding an area with postal code by default.
* **Shipping Method** - Shipping provider like DHL, UPC, DPD
* **Shipping Rate** - List of shipping methods with prices offered by shipping method for specified delivery address.

## Application

### Query Ports

* **ProductMinimalShippingRate** - gets a rate with minimal shipping price for one product and session shipping address.
* **ProductShippingRates** - gets all shipping rates for one product and session shipping address.
* **SessionShippingAddress** - gets information about user session shipping address and customer addresses list to pick another session address.

### Use Case Ports

* **SetSessionShippingAddress** - sets new session shipping address. Can accept either existing customer address or find a shipping area by provided postal code.

## Supported PHP versions:

* 7.3
* 7.4
* 8.0
