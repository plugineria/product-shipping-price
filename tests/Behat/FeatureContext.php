<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Behat;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert;
use Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress\SetSessionShippingAddressCommand;
use Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress\SetSessionShippingAddressResult;
use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\Domain\Model\Address\DefaultAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\DefaultCustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\DefaultCustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\DefaultCustomerShippingAddressId;
use Plugineria\ProductShippingPrice\Domain\Model\DefaultProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\DefaultShippingRate;
use Plugineria\ProductShippingPrice\Tests\Integration\TestApp;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\ShippingMethodTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\ShippingRateTestBuilder;

class FeatureContext implements Context
{
    /** @var TestApp */
    private $app;

    /** @var ShippingRateView|null */
    private $minimalShippingRate;

    /** @var ShippingRateView[]|null */
    private $productShippingRates;

    /** @var SetSessionShippingAddressResult|null */
    private $setSessionShippingAddressResult;

    public function __construct()
    {
        $this->app = TestApp::create();
    }

    /**
     * @Given user is logged in as :username
     */
    public function userIsLoggedInAs(string $username)
    {
        $this->app->customerSessionResolver->setCustomerId(new DefaultCustomerId($username));
    }

    /**
     * @Given user :username has shipping addresses:
     */
    public function userHasShippingAddresses(string $username, TableNode $tableShippingAddresses): void
    {
        foreach ($tableShippingAddresses as $item) {
            $this->app->customerShippingAddressRepository->save(new DefaultCustomerShippingAddress(
                new DefaultCustomerShippingAddressId($item['id']),
                AddressTestBuilder::create()
                    ->setCity($item['city'])
                    ->setPostalCode($item['postal_code'])
                    ->setRegion($item['region'] ?? null)
                    ->setCountry($item['country'])
                    ->setStreetAddress($item['street_address'])
                    ->build(),
                new DefaultCustomerId($username)
            ));
        }
    }

    /**
     * @Given user :username has primary shipping address :addressId
     */
    public function userHasPrimaryShippingAddress(string $username, string $addressId)
    {
        $this->app->customerShippingAddressResolver->setDefaultCustomerShippingAddress(
            $this->app->customerShippingAddressRepository->getById(new DefaultCustomerShippingAddressId($addressId))
        );
    }

    /**
     * @Given shipping methods are available for :productId shipped to :country, :region, :postalCode, :city, :street:
     */
    public function shippingMethodsAreAvailableForProductShippedTo(
        string $productId,
        string $country,
        string $region,
        string $postalCode,
        string $city,
        string $street,
        TableNode $tableShippingMethods
    ): void {
        $shippingMethods = [];
        foreach ($tableShippingMethods as $item) {
            $shippingMethod = ShippingMethodTestBuilder::create()
                ->setId(new DefaultShippingMethodId($item['id']))
                ->setTitle($item['title'])
                ->build();
            $shippingMethods[] = $shippingMethod;
        }
        $this->app->availableShippingMethodsResolver->setShippingMethods(...$shippingMethods);
    }

    /**
     * @Given shipping rates for :productId shipped to :country, :region, :postCode, :city, :street with :methodId are:
     */
    public function shippingRatesForShippedToWithAre(
        string $productId,
        string $country,
        string $region,
        string $postCode,
        string $city,
        string $streetAddress,
        string $methodId,
        TableNode $shippingRates
    ): void {
        $methodId = new DefaultShippingMethodId($methodId);

        $this->app->priceCalculator->setRatesPerShippingMethod(
            $methodId,
            ...array_map(
                static function (array $shippingRate) use ($methodId): DefaultShippingRate {
                    return ShippingRateTestBuilder::create()
                        ->setShippingMethodId($methodId)
                        ->setCode($shippingRate['code'])
                        ->setTitle($shippingRate['title'])
                        ->setPrice((float)$shippingRate['price'])
                        ->setDescription($shippingRate['description'])
                        ->build();
                },
                (array)$shippingRates->getIterator()
            )
        );
    }

    /**
     * @When user opens :productId page
     */
    public function userOpensPage(string $productId)
    {
        $this->minimalShippingRate = $this->app->productMinimalShippingRateQuery->execute(
            new DefaultProductId($productId)
        );
    }

    /**
     * @Then shipping rate :methodId :code with title :methodTitle :rateTitle, price :price and :desc is displayed
     */
    public function shippingRateWithTitlePriceAndIsDisplayed(
        string $methodId,
        string $code,
        string $methodTitle,
        string $rateTitle,
        string $price,
        string $desc
    ): void {
        Assert::assertEquals(
            new ShippingRateView(
                ShippingMethodTestBuilder::create()
                    ->setId(new DefaultShippingMethodId($methodId))
                    ->setTitle($methodTitle)
                    ->build(),
                (float)$price,
                $code,
                $rateTitle,
                $desc,
            ),
            $this->minimalShippingRate
        );
    }

    /**
     * @When user opens all shipping rates for :productId
     */
    public function userOpensAllShippingRatesFor(string $productId)
    {
        $this->productShippingRates = $this->app->productShippingRatesQuery->execute(new DefaultProductId($productId));
    }

    /**
     * @Then shipping rates are displayed:
     */
    public function shippingRatesAreDisplayed(TableNode $tableShippingRates)
    {
        $expectedShippingRates = [];

        foreach ($tableShippingRates as $shippingRate) {
            $expectedShippingRates[] = new ShippingRateView(
                ShippingMethodTestBuilder::create()
                    ->setId(new DefaultShippingMethodId($shippingRate['shipping_method_id']))
                    ->setTitle($shippingRate['shipping_method_title'])
                    ->build(),
                (float)$shippingRate['price'],
                $shippingRate['shipping_rate_code'],
                $shippingRate['shipping_rate_title'],
                $shippingRate['description'],
            );
        }

        Assert::assertEquals(
            $expectedShippingRates,
            $this->productShippingRates
        );
    }

    /**
     * @When user sets session shipping address to :customerShippingAddressId
     */
    public function userSetsSessionShippingAddressTo(string $customerShippingAddressId): void
    {
        $this->setSessionShippingAddressResult = $this->app->setSessionShippingAddressUseCase->execute(
            new SetSessionShippingAddressCommand(
                null,
                null,
                new DefaultCustomerShippingAddressId($customerShippingAddressId)
            )
        );
    }

    /**
     * @Then user session address becomes :country, :region, :postalCode, :city, :street
     * @Then user session address becomes :country, :region, :postalCode, :city
     * @Then user session address becomes :country, :postalCode, :city
     */
    public function userSessionAddressBecomes(
        string $country,
        string $postalCode,
        string $city,
        ?string $street = null,
        ?string $region = null
    ): void {
        Assert::assertEquals(
            new DefaultAddress(
                $country,
                $city,
                $postalCode,
                $street,
                $region
            ),
            $this->app->sessionShippingAddressQuery->execute()->getAddress()
        );
    }

    /**
     * @Then such shipping address does not exist
     */
    public function suchShippingAddressDoesNotExist(): void
    {
        Assert::assertFalse($this->setSessionShippingAddressResult->isValid());
        Assert::assertEquals(
            SetSessionShippingAddressResult::CUSTOMER_ADDRESS_ID_NOT_FOUND,
            $this->setSessionShippingAddressResult->getError()
        );
    }

    /**
     * @Given example shipping address is :country, :region, :postalCode, :city, :street
     */
    public function exampleShippingAddressIs(
        string $country,
        string $region,
        string $postalCode,
        string $city,
        string $street
    ): void {
        $this->app->exampleShippingAddressResolver->setExampleShippingAddress(new DefaultAddress(
            $country,
            $city,
            $postalCode,
            $street,
            $region,
        ));
    }

    /**
     * @Given postal code :postalCode is located in :city
     */
    public function postalCodeIsLocatedIn(string $postalCode, string $city): void
    {
        $this->app->postalCodeAddressFactory->setCity($city);
    }

    /**
     * @Given user is not logged in
     */
    public function userIsNotLoggedIn()
    {
        $this->app->customerSessionResolver->setCustomerId(null);
    }

    /**
     * @When user sets session shipping postal code to :postalCode
     */
    public function userSetsSessionShippingPostalCodeTo(string $postalCode): void
    {
        $this->setSessionShippingAddressResult = $this->app->setSessionShippingAddressUseCase->execute(
            new SetSessionShippingAddressCommand(
                null,
                $postalCode,
                null
            )
        );
    }

    /**
     * @Then such postal code does not exist
     */
    public function suchPostalCodeDoesNotExist()
    {
        Assert::assertFalse($this->setSessionShippingAddressResult->isValid());
        Assert::assertEquals(
            SetSessionShippingAddressResult::POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY,
            $this->setSessionShippingAddressResult->getError()
        );
    }
}
