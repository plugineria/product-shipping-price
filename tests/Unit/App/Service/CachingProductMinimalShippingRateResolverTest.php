<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Unit\App\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\App\Service\CachingProductMinimalShippingRateResolver;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\ProductMinimalShippingRateResolver;
use Plugineria\ProductShippingPrice\Infrastructure\InMemory\Service\InMemoryCache;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\ShippingRateTestBuilder;

use function md5;
use function serialize;
use function unserialize;

class CachingProductMinimalShippingRateResolverTest extends TestCase
{
    private const TTL = 1;

    /** @var CachingProductMinimalShippingRateResolver */
    private $cachingProductMinimalShippingRateResolver;

    /** @var ProductMinimalShippingRateResolver | MockObject */
    private $realResolverMock;

    /** @var InMemoryCache */
    private $cache;

    protected function setUp(): void
    {
        parent::setUp();

        $this->realResolverMock = $this->createMock(ProductMinimalShippingRateResolver::class);
        $this->cache = new InMemoryCache();

        $this->cachingProductMinimalShippingRateResolver = new CachingProductMinimalShippingRateResolver(
            $this->realResolverMock,
            $this->cache,
            self::TTL
        );
    }

    public function testDoNotCacheWhenShippingAddressNotProvided(): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingRate = ShippingRateTestBuilder::create()->build();

        $this->realResolverMock
            ->expects(self::once())
            ->method('getMinimalShippingRate')
            ->with($productId, null)
            ->willReturn($shippingRate);

        // Act + assert
        self::assertSame(
            $shippingRate,
            $this->cachingProductMinimalShippingRateResolver->getMinimalShippingRate($productId)
        );
    }

    public function testSaveCacheWithShippingAddressProvided(): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingAddress = AddressTestBuilder::create()->build();
        $shippingRate = ShippingRateTestBuilder::create()->build();
        $shippingAddressHash = md5(serialize($shippingAddress));

        $this->realResolverMock
            ->expects(self::once())
            ->method('getMinimalShippingRate')
            ->with($productId, $shippingAddress)
            ->willReturn($shippingRate);

        // Act
        $result = $this->cachingProductMinimalShippingRateResolver->getMinimalShippingRate(
            $productId,
            $shippingAddress
        );

        // Assert
        self::assertSame($shippingRate, $result);
        self::assertEquals(
            $shippingRate,
            unserialize(
                $this->cache->get("widgento.product_shipping_price.min_price.$productId.$shippingAddressHash")
            )
        );
    }

    public function testUseCachedValueForSecondCall(): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingAddress = AddressTestBuilder::create()->build();
        $shippingRate = ShippingRateTestBuilder::create()->build();

        $this->realResolverMock
            ->expects(self::once())
            ->method('getMinimalShippingRate')
            ->with($productId, $shippingAddress)
            ->willReturn($shippingRate);

        // Act
        $this->cachingProductMinimalShippingRateResolver->getMinimalShippingRate($productId, $shippingAddress);
        $secondResult = $this->cachingProductMinimalShippingRateResolver->getMinimalShippingRate(
            $productId,
            $shippingAddress
        );

        // Assert
        self::assertNotSame($shippingRate, $secondResult);
        self::assertEquals($shippingRate, $secondResult);
    }

    public function testCallRealResolverAfterCacheExpiredOnSecondCall(): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingAddress = AddressTestBuilder::create()->build();
        $shippingRate = ShippingRateTestBuilder::create()->build();

        $this->realResolverMock
            ->expects(self::exactly(2))
            ->method('getMinimalShippingRate')
            ->with($productId, $shippingAddress)
            ->willReturn($shippingRate);
        $this->cachingProductMinimalShippingRateResolver->getMinimalShippingRate($productId, $shippingAddress);
        sleep(self::TTL + 1);

        // Act
        $secondResult = $this->cachingProductMinimalShippingRateResolver->getMinimalShippingRate(
            $productId,
            $shippingAddress
        );

        // Assert
        self::assertEquals($shippingRate, $secondResult);
    }
}
