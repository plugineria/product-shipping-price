<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Unit\Domain\Model\Address;

use JsonException;
use Plugineria\ProductShippingPrice\Domain\Exception\AddressAttributeNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\Address\DefaultAddress;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;

class DefaultAddressTest extends TestCase
{
    private const COUNTRY = 'US';
    private const REGION = 'TX';
    private const CITY = 'Austin';
    private const POSTAL_CODE = '00111';
    private const STREET = 'Main st 2020';

    public function testSerializingAddressToString(): void
    {
        // Arrange
        $address = AddressTestBuilder::create()
            ->setCountry(self::COUNTRY)
            ->setRegion(self::REGION)
            ->setCity(self::CITY)
            ->setPostalCode(self::POSTAL_CODE)
            ->setStreetAddress(self::STREET)
            ->build();

        // Act + assert
        self::assertEquals(
            json_encode([
                DefaultAddress::COUNTRY => self::COUNTRY,
                DefaultAddress::CITY => self::CITY,
                DefaultAddress::POSTAL_CODE => self::POSTAL_CODE,
                DefaultAddress::STREET_ADDRESS => self::STREET,
                DefaultAddress::REGION => self::REGION,
            ]),
            (string)$address
        );
    }

    public function testCreateFromJsonWithAllValuesGetsNewObject(): void
    {
        // Arrange
        $json = json_encode([
            DefaultAddress::COUNTRY => self::COUNTRY,
            DefaultAddress::CITY => self::CITY,
            DefaultAddress::POSTAL_CODE => self::POSTAL_CODE,
            DefaultAddress::STREET_ADDRESS => self::STREET,
            DefaultAddress::REGION => self::REGION,
        ]);

        // Act + assert
        self::assertEquals(
            new DefaultAddress(
                self::COUNTRY,
                self::CITY,
                self::POSTAL_CODE,
                self::STREET,
                self::REGION,
            ),
            DefaultAddress::createFromJson($json)
        );
    }

    public function testCreateFromJsonWithMinimalValuesGetsNewObject(): void
    {
        // Arrange
        $json = json_encode([
            DefaultAddress::COUNTRY => self::COUNTRY,
            DefaultAddress::CITY => self::CITY,
            'hello' => 'buye',
        ]);

        // Act + assert
        self::assertEquals(
            new DefaultAddress(
                self::COUNTRY,
                self::CITY,
            ),
            DefaultAddress::createFromJson($json)
        );
    }

    public function testCreateFromJsonWithInvalidJsonThrowsError(): void
    {
        // Arrange
        $invalidJson = '{{{""';

        // Assert
        self::expectException(JsonException::class);

        // Act
        DefaultAddress::createFromJson($invalidJson);
    }

    public function testCreateFromJsonWithoutCountryThrowsException(): void
    {
        // Arrange
        $json = json_encode([
            DefaultAddress::CITY => self::CITY,
            'hello' => 'buye',
        ]);

        // Assert
        self::expectException(AddressAttributeNotFound::class);

        // Act + assert
        DefaultAddress::createFromJson($json);
    }

    public function testCreateFromJsonWithoutCityThrowsException(): void
    {
        // Arrange
        $json = json_encode([
            DefaultAddress::COUNTRY => self::COUNTRY,
            'hello' => 'buye',
        ]);

        // Assert
        self::expectException(AddressAttributeNotFound::class);

        // Act + assert
        DefaultAddress::createFromJson($json);
    }
}
