<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Unit\Domain\Service\Customer;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Service\CustomerSessionResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\CustomerShippingAddressResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\DefaultShippingAddressResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\ExampleShippingAddressResolver;
use Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository\InMemorySessionShippingAddressRepository;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\CustomerShippingAddressTestBuilder;

class DefaultShippingAddressResolverTest extends TestCase
{
    /** @var CustomerShippingAddressResolver */
    private $customerShippingAddressResolver;

    /** @var CustomerSessionResolver | MockObject */
    private $customerSessionResolverMock;

    /** @var CustomerShippingAddressResolver | MockObject */
    private $customerShippingAddressResolverMock;

    /** @var InMemorySessionShippingAddressRepository */
    private $sessionShippingAddressRepository;

    /** @var Address */
    private $exampleShippingAddress;

    protected function setUp(): void
    {
        $this->customerShippingAddressResolverMock = $this->createMock(CustomerShippingAddressResolver::class);
        $this->customerSessionResolverMock = $this->createMock(CustomerSessionResolver::class);
        $this->exampleShippingAddress = AddressTestBuilder::create()->build();
        $exampleShippingAddressResolver = $this->createMock(ExampleShippingAddressResolver::class);
        $exampleShippingAddressResolver
            ->method('getExampleShippingAddress')
            ->willReturn($this->exampleShippingAddress);
        $this->sessionShippingAddressRepository = new InMemorySessionShippingAddressRepository();

        $this->customerShippingAddressResolver = new DefaultShippingAddressResolver(
            $this->sessionShippingAddressRepository,
            $this->customerShippingAddressResolverMock,
            $this->customerSessionResolverMock,
            $exampleShippingAddressResolver
        );
    }

    public function testGetExampleAddressForEmptyCustomerSession(): void
    {
        // Arrange
        $this->customerSessionResolverMock
            ->method('getCustomerId')
            ->willReturn(null);

        // Act + Assert
        self::assertSame(
            $this->exampleShippingAddress,
            $this->customerShippingAddressResolver->getDefaultShippingAddress()
        );
        self::assertNull($this->sessionShippingAddressRepository->get());
    }

    public function testGetExampleAddressForCustomerWithoutAddress(): void
    {
        // Arrange
        $customerId = $this->createMock(CustomerId::class);
        $this->customerSessionResolverMock
            ->method('getCustomerId')
            ->willReturn($customerId);

        $this->customerShippingAddressResolverMock
            ->expects(self::once())
            ->method('getPrimaryCustomerShippingAddress')
            ->with($customerId)
            ->willReturn(null);

        // Act + Assert
        self::assertEquals(
            $this->exampleShippingAddress,
            $this->customerShippingAddressResolver->getDefaultShippingAddress()
        );
        self::assertNull($this->sessionShippingAddressRepository->get());
    }

    public function testGetDefaultCustomerAddressAndSaveInSessionRepository(): void
    {
        // Arrange
        $customerId = $this->createMock(CustomerId::class);
        $customerShippingAddress = CustomerShippingAddressTestBuilder::create()->build();
        $this->customerSessionResolverMock
            ->method('getCustomerId')
            ->willReturn($customerId);

        $this->customerShippingAddressResolverMock
            ->method('getPrimaryCustomerShippingAddress')
            ->with($customerId)
            ->willReturn($customerShippingAddress);

        // Act + Assert
        self::assertEquals(
            $customerShippingAddress->getAddress(),
            $this->customerShippingAddressResolver->getDefaultShippingAddress()
        );
        self::assertEquals(
            $customerShippingAddress->getAddress(),
            $this->sessionShippingAddressRepository->get()
        );
    }

    public function testGetShippingAddressRecordedInSessionAddressRepository(): void
    {
        // Arrange
        $address = AddressTestBuilder::create()->build();
        $this->sessionShippingAddressRepository->save($address);

        // Act + Assert
        self::assertEquals(
            $address,
            $this->customerShippingAddressResolver->getDefaultShippingAddress()
        );
    }
}
