<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Unit\Domain\Service\ShippingRate;

use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\ProductShippingRatesResolver;
use Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository\InMemoryShippingMethodRepository;
use Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\MockedAvailableShippingMethodsResolver;
use Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\ShippingRate\MockedPriceCalculator;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\ShippingMethodTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\ShippingRateTestBuilder;

class ProductShippingRatesResolverTest extends TestCase
{
    /** @var ProductShippingRatesResolver */
    private $productShippingRatesResolver;

    /** @var MockedAvailableShippingMethodsResolver */
    private $availableShippingMethodsResolverMock;

    /** @var MockedPriceCalculator */
    private $priceCalculatorMock;

    protected function setUp(): void
    {
        $this->availableShippingMethodsResolverMock = new MockedAvailableShippingMethodsResolver(
            new InMemoryShippingMethodRepository()
        );
        $this->priceCalculatorMock = new MockedPriceCalculator();

        $this->productShippingRatesResolver = new ProductShippingRatesResolver(
            $this->availableShippingMethodsResolverMock,
            $this->priceCalculatorMock,
        );
    }

    public function testGetProductShippingRates(): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingMethod1 = ShippingMethodTestBuilder::create()
            ->setId(new DefaultShippingMethodId('method1'))
            ->build();
        $shippingMethod2 = ShippingMethodTestBuilder::create()
            ->setId(new DefaultShippingMethodId('method2'))
            ->build();
        $method1rate1 = ShippingRateTestBuilder::create()
                ->setShippingMethodId($shippingMethod1->getId())
                ->setTitle('method1_rate1')
                ->build();
        $method1rate2 = ShippingRateTestBuilder::create()
                ->setShippingMethodId($shippingMethod1->getId())
                ->setTitle('method1_rate2')
                ->build();
        $method2rate1 = ShippingRateTestBuilder::create()
                ->setShippingMethodId($shippingMethod2->getId())
                ->setTitle('method2_rate1')
                ->build();
        $method2rate2 = ShippingRateTestBuilder::create()
                ->setShippingMethodId($shippingMethod2->getId())
                ->setTitle('method2_rate2')
                ->build();
        $shippingAddress = AddressTestBuilder::create()->build();
        $this->availableShippingMethodsResolverMock->setShippingMethods(
            $shippingMethod1,
            $shippingMethod2,
        );
        $this->priceCalculatorMock
            ->setRatesPerShippingMethod($shippingMethod1->getId(), $method1rate1, $method1rate2)
            ->setRatesPerShippingMethod($shippingMethod2->getId(), $method2rate1, $method2rate2);
        $expectedRates = [$method1rate1, $method1rate2, $method2rate1, $method2rate2];

        // Act
        $rates = $this->productShippingRatesResolver->getProductShippingRates($productId, $shippingAddress);

        // Assert
        self::assertEquals($expectedRates, $rates);
    }
}
