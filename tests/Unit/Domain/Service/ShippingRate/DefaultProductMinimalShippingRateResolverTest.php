<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Unit\Domain\Service\ShippingRate;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;
use Plugineria\ProductShippingPrice\Domain\Service\AvailableShippingMethodsResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\DefaultProductMinimalShippingRateResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\MinimalRateCalculator;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\ShippingMethodTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\ShippingRateTestBuilder;

class DefaultProductMinimalShippingRateResolverTest extends TestCase
{
    /** @var DefaultProductMinimalShippingRateResolver */
    private $productMinimalShippingRateResolver;

    /** @var AvailableShippingMethodsResolver | MockObject */
    private $availableShippingMethodsResolverMock;

    /** @var MinimalRateCalculator | MockObject */
    private $minimalRateCalculatorMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->availableShippingMethodsResolverMock = $this->createMock(
            AvailableShippingMethodsResolver::class
        );
        $this->minimalRateCalculatorMock = $this->createMock(MinimalRateCalculator::class);
        $this->productMinimalShippingRateResolver = new DefaultProductMinimalShippingRateResolver(
            $this->availableShippingMethodsResolverMock,
            $this->minimalRateCalculatorMock,
        );
    }

    public function testGetEmptyWithNoShippingMethods(): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingAddress = AddressTestBuilder::create()->build();

        // Act + Assert
        self::assertNull(
            $this->productMinimalShippingRateResolver->getMinimalShippingRate($productId, $shippingAddress)
        );
    }

    public function testGetWithThreeShippingMethodsOneDoesNotHaveRate(): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingAddress = AddressTestBuilder::create()->build();
        $shippingMethods = [
            ShippingMethodTestBuilder::create()
                ->setId(new DefaultShippingMethodId('sm1'))
                ->build(),
            ShippingMethodTestBuilder::create()
                ->setId(new DefaultShippingMethodId('sm2'))
                ->build(),
            ShippingMethodTestBuilder::create()
                ->setId(new DefaultShippingMethodId('sm3'))
                ->build(),
        ];
        $shippingRates = [
            ShippingRateTestBuilder::create()
                ->setShippingMethodId($shippingMethods[0]->getId())
                ->setPrice(10)
                ->build(),
            ShippingRateTestBuilder::create()
                ->setShippingMethodId($shippingMethods[1]->getId())
                ->setPrice(5)
                ->build(),
            null,
        ];
        $this->availableShippingMethodsResolverMock
            ->method('getAvailableShippingMethods')
            ->with($productId, $shippingAddress)
            ->willReturn($shippingMethods);

        $this->minimalRateCalculatorMock
            ->method('calculate')
            ->withConsecutive(...array_map(
                static function (ShippingMethod $shippingMethod) use ($productId, $shippingAddress): array {
                    return [$productId, $shippingMethod->getId(), $shippingAddress];
                },
                $shippingMethods
            ))
            ->willReturnOnConsecutiveCalls(...$shippingRates);

        // Act + Assert
        self::assertEquals(
            $shippingRates[1],
            $this->productMinimalShippingRateResolver->getMinimalShippingRate($productId, $shippingAddress)
        );
    }
}
