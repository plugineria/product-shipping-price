<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Unit\Domain\Service\ShippingMethod;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\MinimalRateCalculator;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\PriceCalculator;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\ShippingRateTestBuilder;

class MinimalRateCalculatorTest extends TestCase
{
    /** @var MinimalRateCalculator */
    private $minimalRateCalculator;

    /** @var PriceCalculator | MockObject */
    private $priceCalculatorMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->priceCalculatorMock = $this->createMock(PriceCalculator::class);
        $this->minimalRateCalculator = new MinimalRateCalculator($this->priceCalculatorMock);
    }

    public function testGetNullWhenNoShippingRates(): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingMethodId = new DefaultShippingMethodId('sm');
        $shippingAddress = AddressTestBuilder::create()->build();

        $this->priceCalculatorMock
            ->method('getRatesPerShippingMethod')
            ->with($shippingMethodId, $productId, $shippingAddress)
            ->willReturn([]);

        // Act + assert
        self::assertNull($this->minimalRateCalculator->calculate($productId, $shippingMethodId, $shippingAddress));
    }

    public function dataRates(): iterable
    {
        yield [
            ShippingRateTestBuilder::create()->build(),
            ShippingRateTestBuilder::create()->build(),
        ];

        yield [
            ShippingRateTestBuilder::create()->setPrice(5)->build(),
            ShippingRateTestBuilder::create()->setPrice(5)->build(),
            ShippingRateTestBuilder::create()->setPrice(5)->build(),
            ShippingRateTestBuilder::create()->setPrice(10)->build(),
        ];
    }

    /**
     * @dataProvider dataRates
     */
    public function testRates(ShippingRate $expectedMinimalShippingRate, ShippingRate ...$shippingRates): void
    {
        // Arrange
        $productId = $this->createMock(ProductId::class);
        $shippingMethodId = new DefaultShippingMethodId('sm');
        $shippingAddress = AddressTestBuilder::create()->build();

        $this->priceCalculatorMock
            ->method('getRatesPerShippingMethod')
            ->with($shippingMethodId, $productId, $shippingAddress)
            ->willReturn($shippingRates);

        // Act + assert
        self::assertEquals(
            $expectedMinimalShippingRate,
            $this->minimalRateCalculator->calculate($productId, $shippingMethodId, $shippingAddress),
        );
    }
}
