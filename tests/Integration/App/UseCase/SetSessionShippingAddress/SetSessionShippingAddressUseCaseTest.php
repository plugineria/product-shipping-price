<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Integration\App\UseCase\SetSessionShippingAddress;

use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress\SetSessionShippingAddressCommand;
use Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress\SetSessionShippingAddressResult;
use Plugineria\ProductShippingPrice\Tests\Integration\TestApp;

class SetSessionShippingAddressUseCaseTest extends TestCase
{
    /** @var TestApp */
    private $app;

    protected function setUp(): void
    {
        $this->app = TestApp::create();
    }

    public function testAddressNotSpecified(): void
    {
        // Act
        $result = $this->app->setSessionShippingAddressUseCase->execute(new SetSessionShippingAddressCommand());

        // Assert
        self::assertFalse($result->isValid());
        self::assertEquals(
            SetSessionShippingAddressResult::ADDRESS_NOT_SPECIFIED,
            $result->getError()
        );
    }
}
