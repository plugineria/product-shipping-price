<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Integration\App\Query;

use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\App\View\SessionShippingAddressView;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\DefaultCustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\DefaultCustomerShippingAddressId;
use Plugineria\ProductShippingPrice\Tests\Integration\TestApp;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;

class SessionShippingAddressQueryTest extends TestCase
{
    /** @var TestApp */
    private $app;

    protected function setUp(): void
    {
        $this->app = TestApp::create();
        $this->app->exampleShippingAddressResolver->setExampleShippingAddress(
            AddressTestBuilder::create()
                ->setPostalCode('11-111')
                ->setCity('Example City')
                ->setStreetAddress('Example Street')
                ->build()
        );
    }

    public function testCustomerRegisteredAndAddressProvidedByPostalCode(): void
    {
        // Arrange
        $customerId = $this->createMock(CustomerId::class);
        $sessionAddress = AddressTestBuilder::create()->setPostalCode('00-001')->build();
        $this->app->customerSessionResolver->setCustomerId($customerId);
        $customerShippingAddress1 = new DefaultCustomerShippingAddress(
            new DefaultCustomerShippingAddressId('address1'),
            AddressTestBuilder::create()->setPostalCode('00-002')->build(),
            $customerId
        );
        $this->app->customerShippingAddressRepository->save($customerShippingAddress1);
        $this->app->sessionShippingAddressRepository->save($sessionAddress);

        // Act
        $result = $this->app->sessionShippingAddressQuery->execute();

        // Assert
        self::assertEquals(
            new SessionShippingAddressView(
                $sessionAddress,
                null,
                [$customerShippingAddress1],
                true
            ),
            $result
        );
    }

    public function testCustomerRegisteredAndSavedCustomerShippingAddress(): void
    {
        // Arrange
        $customerId = $this->createMock(CustomerId::class);
        $sessionAddress = AddressTestBuilder::create()->setPostalCode('00-001')->build();
        $this->app->customerSessionResolver->setCustomerId($customerId);
        $customerShippingAddress1 = new DefaultCustomerShippingAddress(
            new DefaultCustomerShippingAddressId('address1'),
            AddressTestBuilder::create()->setPostalCode('00-002')->build(),
            $customerId
        );
        $customerShippingAddress2 = new DefaultCustomerShippingAddress(
            new DefaultCustomerShippingAddressId('address2'),
            $sessionAddress,
            $customerId
        );
        $this->app->customerShippingAddressRepository->save($customerShippingAddress1);
        $this->app->customerShippingAddressRepository->save($customerShippingAddress2);
        $this->app->sessionShippingAddressRepository->save($sessionAddress);

        // Act
        $result = $this->app->sessionShippingAddressQuery->execute();

        // Assert
        self::assertEquals(
            new SessionShippingAddressView(
                $sessionAddress,
                $customerShippingAddress2->getId(),
                [$customerShippingAddress1, $customerShippingAddress2],
                true
            ),
            $result
        );
    }

    public function testCustomerRegisteredDoesNotHaveAddressesExampleAddressReturned(): void
    {
        // Arrange
        $customerId = $this->createMock(CustomerId::class);
        $this->app->customerSessionResolver->setCustomerId($customerId);

        // Act
        $result = $this->app->sessionShippingAddressQuery->execute();

        // Assert
        self::assertEquals(
            new SessionShippingAddressView(
                $this->app->exampleShippingAddressResolver->getExampleShippingAddress(),
                null,
                [],
                false
            ),
            $result
        );
    }

    public function testCustomerNotRegisteredAndExampleAddressReturned(): void
    {
        // Act
        $result = $this->app->sessionShippingAddressQuery->execute();

        // Assert
        self::assertEquals(
            new SessionShippingAddressView(
                $this->app->exampleShippingAddressResolver->getExampleShippingAddress(),
                null,
                [],
                false
            ),
            $result
        );
    }

    public function testCustomerNotRegisteredAndAddressProvidedByPostalCode(): void
    {
        // Arrange
        $sessionAddress = AddressTestBuilder::create()->setPostalCode('00-001')->build();
        $this->app->sessionShippingAddressRepository->save($sessionAddress);

        // Act
        $result = $this->app->sessionShippingAddressQuery->execute();

        // Assert
        self::assertEquals(
            new SessionShippingAddressView(
                $sessionAddress,
                null,
                [],
                true
            ),
            $result
        );
    }
}
