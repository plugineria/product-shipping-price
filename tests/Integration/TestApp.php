<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Integration;

use Plugineria\ProductShippingPrice\App\Query\ProductMinimalShippingRateQuery;
use Plugineria\ProductShippingPrice\App\Query\ProductShippingRatesQuery;
use Plugineria\ProductShippingPrice\App\Query\SessionShippingAddressQuery;
use Plugineria\ProductShippingPrice\App\UseCase\SetSessionShippingAddress\SetSessionShippingAddressUseCase;
use Plugineria\ProductShippingPrice\App\View\ShippingRateViewFactory;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\DefaultShippingAddressResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\SessionCustomerShippingAddressUpdater;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\SessionPostalCodeAddressUpdater;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\DefaultProductMinimalShippingRateResolver;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\MinimalRateCalculator;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\ProductShippingRatesResolver;
use Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository\InMemoryCustomerShippingAddressRepository;
use Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository\InMemorySessionShippingAddressRepository;
use Plugineria\ProductShippingPrice\Infrastructure\InMemory\Repository\InMemoryShippingMethodRepository;
use Plugineria\ProductShippingPrice\Infrastructure\Mock\Factory\MockedPostalCodeAddressFactory;
use Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\MockedAvailableShippingMethodsResolver;
use Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\MockedCustomerSessionResolver;
use Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\ShippingAddress\MockedCustomerShippingAddressResolver;
use Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\ShippingAddress\MockedExampleShippingAddressResolver;
use Plugineria\ProductShippingPrice\Infrastructure\Mock\Service\ShippingRate\MockedPriceCalculator;

class TestApp
{
    /** @var ProductMinimalShippingRateQuery */
    public $productMinimalShippingRateQuery;

    /** @var ProductShippingRatesQuery */
    public $productShippingRatesQuery;

    /** @var SessionShippingAddressQuery */
    public $sessionShippingAddressQuery;

    /** @var SetSessionShippingAddressUseCase */
    public $setSessionShippingAddressUseCase;

    /** @var MockedCustomerSessionResolver */
    public $customerSessionResolver;

    /** @var MockedPriceCalculator */
    public $priceCalculator;

    /** @var MockedAvailableShippingMethodsResolver */
    public $availableShippingMethodsResolver;

    /** @var MockedCustomerShippingAddressResolver */
    public $customerShippingAddressResolver;

    /** @var InMemoryShippingMethodRepository */
    public $shippingMethodRepository;

    /** @var InMemorySessionShippingAddressRepository */
    public $sessionShippingAddressRepository;

    /** @var InMemoryCustomerShippingAddressRepository */
    public $customerShippingAddressRepository;

    /** @var MockedPostalCodeAddressFactory */
    public $postalCodeAddressFactory;

    /** @var MockedExampleShippingAddressResolver */
    public $exampleShippingAddressResolver;

    private function __construct()
    {
        $this->initMocks();

        $defaultShippingAddressResolver = new DefaultShippingAddressResolver(
            $this->sessionShippingAddressRepository,
            $this->customerShippingAddressResolver,
            $this->customerSessionResolver,
            $this->exampleShippingAddressResolver,
        );

        $shippingRateViewFactory = new ShippingRateViewFactory($this->shippingMethodRepository);

        $this->initProductMinimalShippingRateQuery($defaultShippingAddressResolver, $shippingRateViewFactory);
        $this->initProductShippingRatesQuery($defaultShippingAddressResolver, $shippingRateViewFactory);
        $this->initSessionShippingAddressQuery($defaultShippingAddressResolver);
        $this->initSetCustomerShippingAddressUseCase();
    }

    private function initMocks(): void
    {
        $this->customerSessionResolver = new MockedCustomerSessionResolver();
        $this->priceCalculator = new MockedPriceCalculator();
        $this->shippingMethodRepository = new InMemoryShippingMethodRepository();
        $this->availableShippingMethodsResolver = new MockedAvailableShippingMethodsResolver(
            $this->shippingMethodRepository
        );
        $this->customerShippingAddressResolver = new MockedCustomerShippingAddressResolver();
        $this->sessionShippingAddressRepository = new InMemorySessionShippingAddressRepository();
        $this->customerShippingAddressRepository = new InMemoryCustomerShippingAddressRepository();
        $this->postalCodeAddressFactory = new MockedPostalCodeAddressFactory();
        $this->exampleShippingAddressResolver = new MockedExampleShippingAddressResolver();
    }

    private function initProductMinimalShippingRateQuery(
        DefaultShippingAddressResolver $shippingAddressResolver,
        ShippingRateViewFactory $shippingRateViewFactory
    ): void {
        $this->productMinimalShippingRateQuery = new ProductMinimalShippingRateQuery(
            new DefaultProductMinimalShippingRateResolver(
                $this->availableShippingMethodsResolver,
                new MinimalRateCalculator($this->priceCalculator),
            ),
            $shippingAddressResolver,
            $shippingRateViewFactory
        );
    }

    private function initProductShippingRatesQuery(
        DefaultShippingAddressResolver $shippingAddressResolver,
        ShippingRateViewFactory $shippingRateViewFactory
    ): void {
        $this->productShippingRatesQuery = new ProductShippingRatesQuery(
            new ProductShippingRatesResolver(
                $this->availableShippingMethodsResolver,
                $this->priceCalculator,
            ),
            $shippingAddressResolver,
            $shippingRateViewFactory
        );
    }

    public static function create(): self
    {
        return new self();
    }

    private function initSetCustomerShippingAddressUseCase(): void
    {
        $this->setSessionShippingAddressUseCase = new SetSessionShippingAddressUseCase(
            new SessionCustomerShippingAddressUpdater(
                $this->sessionShippingAddressRepository,
                $this->customerShippingAddressRepository,
                $this->customerSessionResolver,
            ),
            new SessionPostalCodeAddressUpdater(
                $this->sessionShippingAddressRepository,
                $this->postalCodeAddressFactory,
                $this->exampleShippingAddressResolver,
            ),
        );
    }

    private function initSessionShippingAddressQuery(DefaultShippingAddressResolver $shippingAddressResolver): void
    {
        $this->sessionShippingAddressQuery = new SessionShippingAddressQuery(
            $shippingAddressResolver,
            $this->customerShippingAddressRepository,
            $this->customerSessionResolver,
            $this->exampleShippingAddressResolver,
        );
    }
}
