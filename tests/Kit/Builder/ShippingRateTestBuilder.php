<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Kit\Builder;

use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\DefaultShippingRate;

class ShippingRateTestBuilder
{
    public const DEFAULT_PRICE = 15.99;
    public const DEFAULT_CODE = 'standard';

    /** @var ShippingMethodId */
    private $shippingMethodId;

    /** @var string */
    private $code;

    /** @var float */
    private $price;

    /** @var string|null */
    private $title;

    /** @var string|null */
    private $description;

    private function __construct()
    {
        $this->shippingMethodId = new DefaultShippingMethodId(ShippingMethodTestBuilder::DEFAULT_ID);
        $this->code = self::DEFAULT_CODE;
        $this->price = self::DEFAULT_PRICE;
        $this->title = null;
        $this->description = null;
    }

    public static function create(): self
    {
        return new self();
    }

    public function build(): DefaultShippingRate
    {
        return new DefaultShippingRate(
            $this->shippingMethodId,
            $this->code,
            $this->price,
            $this->title,
            $this->description,
        );
    }

    public function setShippingMethodId(ShippingMethodId $shippingMethodId): self
    {
        $this->shippingMethodId = $shippingMethodId;

        return $this;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
