<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Kit\Builder;

use Plugineria\ProductShippingPrice\Domain\Model\Address\DefaultAddress;

class AddressTestBuilder
{
    public const COUNTRY = 'PL';
    public const CITY = 'Warszawa';

    /** @var string */
    private $country;

    /** @var string */
    private $city;

    /** @var string | null */
    private $postalCode;

    /** @var string | null */
    private $streetAddress;

    /** @var string | null */
    private $region;

    private function __construct()
    {
        $this->country = self::COUNTRY;
        $this->city = self::CITY;
        $this->postalCode = null;
        $this->streetAddress = null;
        $this->region = null;
    }

    public static function create(): self
    {
        return new self();
    }

    public function build(): DefaultAddress
    {
        return new DefaultAddress(
            $this->country,
            $this->city,
            $this->postalCode,
            $this->streetAddress,
            $this->region,
        );
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function setStreetAddress(?string $streetAddress): self
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }
}
