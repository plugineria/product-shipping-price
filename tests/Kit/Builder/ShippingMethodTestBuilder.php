<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Kit\Builder;

use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethod;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;

class ShippingMethodTestBuilder
{
    public const DEFAULT_ID = 'dpd';
    public const DEFAULT_TITLE = 'DPD';

    /** @var ShippingMethodId */
    private $id;

    /** @var string */
    private $title;

    private function __construct()
    {
        $this->id = new DefaultShippingMethodId(self::DEFAULT_ID);
        $this->title = self::DEFAULT_TITLE;
    }

    public static function create(): self
    {
        return new self();
    }

    public function build(): DefaultShippingMethod
    {
        return new DefaultShippingMethod($this->id, $this->title);
    }

    public function setId(ShippingMethodId $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
