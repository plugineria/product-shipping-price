<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Tests\Kit\Builder;

use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\DefaultCustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddressId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\DefaultCustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\DefaultCustomerShippingAddressId;

use function uniqid;

class CustomerShippingAddressTestBuilder
{
    /** @var CustomerShippingAddressId */
    private $id;

    /** @var Address */
    private $address;

    /** @var CustomerId */
    private $customerId;

    private function __construct()
    {
        $this->id = new DefaultCustomerShippingAddressId(
            uniqid('customer_shipping_address.', true)
        );
        $this->address = AddressTestBuilder::create()->build();
        $this->customerId = new DefaultCustomerId(
            uniqid('customer.', true)
        );
    }

    public static function create(): self
    {
        return new self();
    }

    public function build(): DefaultCustomerShippingAddress
    {
        return new DefaultCustomerShippingAddress(
            $this->id,
            $this->address,
            $this->customerId,
        );
    }

    public function setId(CustomerShippingAddressId $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function setCustomerId(CustomerId $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }
}
